<?php
  
  $answer[7] =
    '
  <h2>Question 7</h2>
  <p>
    In an ecommerce web application you would typically have a series of prices for products stored in the system. The prices may be inclusive or exclusive of taxes, and may need to be combined for various reasons (such as producing a sum total at the end of an online checkout).
  </p>
  
  <p>
    How would you programmatically manipulate those prices, and why?
  </p>
  
  <hr>
  
  <h2>
    As ecommerce example:
  </h2>
  <p>
    WooCommerce stores products in the \'posts\' table of the schema.
  Instead of manually updating the DB, I would use the provided API
  </p>
  <p>From there, you can let WooCommerce handle all of the nitty gritty of updating products, managing the data structure, etc.</p>
  <p>This also allows you to not have updates to WooCommerce break your application if they change their DB layout.</p>
';
?>

