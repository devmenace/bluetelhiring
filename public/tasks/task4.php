<VirtualHost *:80>
  DocumentRoot C:/Users/denissa/PhpstormProjects/Laragon/syncDir/forden/public/
  ServerName forden.loc
  ServerAlias *.forden.loc
  <Directory C:/Users/denissa/PhpstormProjects/Laragon/syncDir/forden/public/>
  AllowOverride All
  Require all granted
  </Directory>
</VirtualHost>

<VirtualHost *:443>
  DocumentRoot C:/Users/denissa/PhpstormProjects/Laragon/syncDir/forden/public/
  ServerName forden.loc
  ServerAlias *.forden.loc
  <Directory C:/Users/denissa/PhpstormProjects/Laragon/syncDir/forden/public/>
  AllowOverride All
  Require all granted
  </Directory>
  
  SSLEngine on
  SSLCertificateFile      C:/laragon/etc/ssl/laragon.crt
  SSLCertificateKeyFile   C:/laragon/etc/ssl/laragon.key

</VirtualHost>
